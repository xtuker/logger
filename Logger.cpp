﻿//**************************************************************************/
// DESCRIPTION: Logger. Insert information with timestamp and decoding error codes Windows NT
// AUTHOR: Falkovski Alexander
// (C)2014
//***************************************************************************/


#include <Logger.h>

#include <Windows.h>
#include <sstream>
#include <iomanip>
#include <codecvt>

ILogger * ILoggable::ILog = NULL; //Init ILog for all child class
Logger * Logger::selfObject = NULL;

Logger::Logger(std::wstring const &LogFile) : LogFileName(LogFile)
{
    isMultiThreadObject = false;
    MainThreadId = ::GetCurrentThreadId();

    is_open = false;
    Open();
}

Logger::~Logger(void)
{
    ofd.close();
    unlock(true);
    CloseHandle(mtx);
}
Logger * Logger::Open()
{
    if(is_open)
        return this;

    lock(true);
    bool CreateBOM = !FileExistsW(LogFileName);

    ofd.open(LogFileName, std::ios_base::app | std::ios_base::binary);
    if(ofd.is_open() == false)
    {
        std::string errorMsg = "Can't open file: ";
        errorMsg.append(WStrToStr(LogFileName).c_str());
        unlock(true);
        throw std::exception(errorMsg.c_str());
    }
    std::locale utf8_locale(std::locale(), new std::codecvt_utf8<wchar_t>);
    ofd.imbue(utf8_locale);

    if(CreateBOM)
    {
        ofd << BOM;
    }
    ofd.flush();
    is_open = true;

    unlock(true);
    
    return this;
}
void Logger::Close()
{
    lock(true);
    ofd.close();
    is_open = false;
    unlock(true);
}
std::wstring Logger::StrToWStr(std::string const &str)
{
    std::wstring wstr;
    size_t contentLength = str.size();
    wstr.resize(contentLength);
    MultiByteToWideChar(CP_ACP, NULL, str.c_str(), static_cast<int>(str.size()), &wstr[0], static_cast<int>(contentLength));
    return wstr;
}
std::string Logger::WStrToStr(std::wstring const &wstr)
{
    std::string str;
    size_t contentLength = wstr.size();
    str.resize(contentLength);
    WideCharToMultiByte(CP_ACP, NULL, wstr.c_str(), static_cast<int>(wstr.size()), &str[0], static_cast<int>(contentLength), NULL, NULL);
    return str;
}
inline bool Logger::FileExistsW(std::wstring const &fileName)
{
    HANDLE hFile = ::CreateFileW(fileName.c_str(), 0, 
                   FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
                   NULL, 
                   OPEN_EXISTING,
                   FILE_FLAG_NO_BUFFERING | FILE_FLAG_SEQUENTIAL_SCAN,
                   NULL);

    if (INVALID_HANDLE_VALUE != hFile)
    {
        ::CloseHandle(hFile);
        return true;
    }
    return false;
}
inline std::wstring Logger::Now()
{
    std::wstringstream ss;
    
    __time64_t long_time;
    tm timeinfo;
    errno_t err;
    
    _time64(&long_time); 
    err = _localtime64_s(&timeinfo, &long_time);
    if(err)
        return std::wstring();

    ss << timeinfo.tm_year+1900 << L"-"
       << std::setfill(L'0') << std::setw(2) << timeinfo.tm_mon+1 << L"-"
       << std::setfill(L'0') << std::setw(2) << timeinfo.tm_mday << L" "
       << std::setfill(L'0') << std::setw(2) << timeinfo.tm_hour << L":"
       << std::setfill(L'0') << std::setw(2) << timeinfo.tm_min << L":"
       << std::setfill(L'0') << std::setw(2) << timeinfo.tm_sec;

    return ss.str();
}
inline std::string Logger::GetMessageErrorA(size_t ErrorCode)
{
    std::stringstream ss;
    ss << "#" << ErrorCode << ": ";
    std::string ret = ss.str();

    LPSTR lpBuffer;
    DWORD nResult = 0;

    if (ErrorCode >= 12000 && ErrorCode <= 12174)
        nResult = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER 
                    | FORMAT_MESSAGE_FROM_HMODULE,
                    GetModuleHandleA("wininet.dll"), 
                    static_cast<DWORD>(ErrorCode),
                    0,
                    (LPSTR)&lpBuffer, 
                    0, NULL);
    else
        nResult = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER
                    | FORMAT_MESSAGE_FROM_SYSTEM 
                    |	FORMAT_MESSAGE_IGNORE_INSERTS, 
                    NULL, 
                    static_cast<DWORD>(ErrorCode), 
                    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                    (LPSTR)&lpBuffer,
                    0, NULL);
    if (nResult)
    {
        if(nResult > 2 && lpBuffer[nResult-1] == '\n' && lpBuffer[nResult-2] == '\r')
            nResult -= 2;
        ret.append(std::string(lpBuffer, &lpBuffer[nResult]));
        LocalFree(lpBuffer);
    }
    return ret;
}
inline std::wstring Logger::GetMessageErrorW(size_t ErrorCode)
{
    std::wstringstream ss;
    ss << L"#" << ErrorCode << L": ";
    std::wstring ret = ss.str();
    

    LPWSTR lpBuffer;
    DWORD nResult = 0;

    if (ErrorCode >= 12000 && ErrorCode <= 12174)
        nResult = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER 
                    | FORMAT_MESSAGE_FROM_HMODULE,
                    GetModuleHandleW(L"wininet.dll"), 
                    static_cast<DWORD>(ErrorCode),
                    0,
                    (LPWSTR)&lpBuffer, 
                    0, NULL);
    else
        nResult = FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER
                    | FORMAT_MESSAGE_FROM_SYSTEM 
                    |	FORMAT_MESSAGE_IGNORE_INSERTS, 
                    NULL,
                    static_cast<DWORD>(ErrorCode),
                    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                    (LPWSTR)&lpBuffer,
                    0, NULL);
    if (nResult)
    {
        if(nResult > 2 && lpBuffer[nResult-1] == L'\n' && lpBuffer[nResult-2] == L'\r')
            nResult -= 2;
        ret.append(std::wstring(lpBuffer, &lpBuffer[nResult]));
        LocalFree(lpBuffer);
    }
    return ret;
}
inline size_t Logger::WriteFile(std::wstring const &msg)
{
    if(!is_open)
        return 0;
    std::wstring message = Now();
    message.append(L" -- ");
    message.append(msg);
    
    lock();
    ofd << message << L"\r\n";
    ofd.flush();
    unlock();
    
    return message.size();
}

size_t Logger::append(std::string const &msg)
{
    return WriteFile(StrToWStr(msg));
}
size_t Logger::append(std::wstring const &msg)
{
    return WriteFile(msg);
}
size_t Logger::append(std::string const &msg, size_t errorCode)
{
    std::string message = msg;
    message.append(" | ");
    message.append(GetMessageErrorA(errorCode));
    return WriteFile(StrToWStr(message));
}
size_t Logger::append(std::wstring const &msg, size_t errorCode)
{
    std::wstring message = msg;
    message.append(L" | ");
    message.append(GetMessageErrorW(errorCode));
    return WriteFile(message);
}

void Logger::CheckThread()
{
    if(MainThreadId != ::GetCurrentThreadId())
        isMultiThreadObject = true;
}
void Logger::lock(bool force)
{
    CheckThread();
    if(isMultiThreadObject || force)
    {
        mtx = CreateMutexW(NULL, FALSE, LogFileName.c_str());
        WaitForSingleObject(mtx, INFINITE);
    }
}
void Logger::unlock(bool force)
{
    if(isMultiThreadObject || force)
    {
        ReleaseMutex(mtx);
    }
}